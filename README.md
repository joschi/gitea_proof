# gitea_proof

This is an OpenPGP proof that connects [my OpenPGP key](https://keyoxide.org/1ea6c4f845272cb75c325a6712143be87aeaa321) to [this Gitea account](https://gitea.com/joschi). For details check out https://keyoxide.org/guides/openpgp-proofs

$argon2id$v=19$m=64,t=512,p=2$zPB9I4xJ6RlwGjIAu6Zf3w$uHGk/6wI8Uz8LhjTdjgcMA